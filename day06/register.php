<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Register</title>
    <style>
	.container{
    	display: flex;
		justify-content: center;
	}
	.form-border{
	    height: 600px;
    	width: 500px;
		border: 1.5px solid #1a89ba;
    }
    .form-group{
        display: flex;
        align-items: center;
        justify-content: flex-start;
        padding: 2%;
        margin-left: 10%;
    }
    .form-label{
        background-color: #6ca834;
        border: 1.5px solid #1a89ba;
        margin-right: 5%;
        padding: 1%;
        color: white;
    }

    .form-label{
        background-color: #6ca834;
        border: 1.5px solid #1a89ba;
        min-width: 18%;
        margin-right: 5%;
        padding: 1%;
        color: white;
    }

    .form-control {
        background-color: #fff;
        border: 1.5px solid #1a89ba;
        display: block;
        width: 52%;
        padding: 0.3rem 0.9rem;
        font-size: 1rem;
        color: #495057;
      }

    .btn-submit{
        background-color: #6ca834;
        border: 1.5px solid #1a89ba;
        width: 30%;
        color: white;
        cursor: pointer;
        margin-left: 140px;
    }
    .form-group input[type="radio"] {
        width: 10%;
    }

    .required-asterisk {
        color: red;
    }

    .error_message {
        color: red;
        margin-left: 60px;
    }

    </style>

</head>

<body>
    <div class="container">
        <div class="form-border">
        <div id="form-error" class="error-message"></div>
            <form action="confirm.php" id="form-login" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name" class="form-label">Họ và tên<span class="required-asterisk">*</span></label>
                    <input type="text" id="name" name="name" class="form-control">
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Giới tính<span class="required-asterisk">*</span></label>
                    <?php
                        $gender = [
                            0 => "Nam",
                            1 => "Nữ",
                        ];
                        $genderKeys = array_keys($gender);
                        $genderValues = array_values($gender);

                        for ($i = 0; $i < count($gender); $i++) {
                            $key = $genderKeys[$i];
                            $value = $genderValues[$i];

                            echo '<input type="radio" id="gender-' . $key . '" value="' . $value . '" class="form-control" name="gender">';
                            echo '<label for="gender-' . $key . '">' . $value . '</label>';
                        }
                    ?>
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Phân khoa<span class="required-asterisk">*</span></label>
                    <select name="major" id="major" class="form-control">
                        <option value="0" selected>--Chọn phân khoa--</option>
                    <?php
                        $major = [
                            "MAT" => "Khoa học máy tính",
                            "KDL" => "Khoa học vật liệu",
                        ];

                        foreach ($major as $key => $value) {
                            echo '<option value="' . $key . '">' . $value . '</option>';
                        }
                    ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="birth" class="form-label">Ngày sinh<span class="required-asterisk">*</span></label>
                    <input type="text" id="birth" name="birth" class="form-control">
                </div>
                <div class="form-group">
                    <label for="address" class="form-label" style ="margin-bottom:70px;">Địa chỉ</label>
                    <textarea type="text" id="address" name="address" class="form-control" style="width: 223px; height: 100px; padding: 1px; margin-top:2px; font-family: Arial; padding: 0.3rem 0.9rem;"></textarea>
                </div>
                <div class="form-group">
                    <label for="img" class="form-label">Hình ảnh</label>
                    <input type="file" id="img" name="img" accept="image/*">
                </div>
                <div class="form-group">
                    <input type="submit" class="btn-submit form-control" value="Đăng ký">
                </div>
            </form>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
        // Regex used to match all possible date in form "dd/mm/yy". Reference: https://stackoverflow.com/questions/15491894/regex-to-validate-date-formats-dd-mm-yyyy-dd-mm-yyyy-dd-mm-yyyy-dd-mmm-yyyy"
        var date_regex =  /^(?:(?:31(\/)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
        $('form').submit(function(event) {
          $(".error_message").remove()
          if ($('#name').val() === '') {
            event.preventDefault();
            $('#form-error').append("<p class='error_message'>Hãy nhập tên.</p>");
          }

          if ($('input[name="gender"]:checked').length === 0) {
            event.preventDefault();
            $('#form-error').append("<p class='error_message'>Hãy nhập giới tính.</p>");
          }

          if ($('#major').val() === '0') {
            event.preventDefault();
            $('#form-error').append("<p class='error_message'>Hãy nhập phân khoa.</p>");
          }

          if ($('#birth').val() === '') {
            event.preventDefault();
            $('#form-error').append("<p class='error_message'>Hãy nhập ngày sinh.</p>");
          }
          else if (!date_regex.test($('#birth').val())) {
            event.preventDefault();
            $('#form-error').append("<p class='error_message'>Hãy nhập ngày sinh đúng định dạng.</p>");
          }
        });
      });
    </script>

</body>
</html>
