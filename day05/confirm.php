<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Confirm</title>
    <style>
	.container{
    	display: flex;
		justify-content: center;
	}
	.form-border{
	    height: 600px;
    	width: 500px;
		border: 1.5px solid #1a89ba;
    }
    .form-group{
        display: flex;
        align-items: center;
        justify-content: flex-start;
        padding: 2%;
        margin-left: 10%;
    }
    .form-label{
        background-color: #6ca834;
        border: 1.5px solid #1a89ba;
        margin-right: 5%;
        padding: 1%;
        color: white;
    }

    .form-label{
        background-color: #6ca834;
        border: 1.5px solid #1a89ba;
        min-width: 18%;
        margin-right: 5%;
        padding: 1%;
        color: white;
    }

    .form-control {
        background-color: #fff;
        border: 1.5px solid #1a89ba;
        display: block;
        width: 52%;
        padding: 0.3rem 0.9rem;
        font-size: 1rem;
        color: #495057;
      }

    .btn-submit{
        background-color: #6ca834;
        border: 1.5px solid #1a89ba;
        width: 30%;
        color: white;
        cursor: pointer;
        margin-left: 140px;
    }
    .form-group input[type="radio"] {
        width: 10%;
    }

    img {
        width: 300px;
        border: 1px solid #ccc;
    }

    </style>
</head>

<body>
    <?php
        // Retrieve form data
        $name = $_POST['name'];
        $gender = $_POST['gender'];
        $major_key = $_POST['major'];
        $birth = $_POST['birth'];
        $address = $_POST['address'];

        $major_value = [
             "MAT" => "Khoa học máy tính",
             "KDL" => "Khoa học vật liệu",
        ];

        $name_response = "<div class='confirmation-message'>" . $name . "</div>";
        $gender_response = "<div class='confirmation-message'>" . $gender . "</div>";
        $major_response = "<div class='confirmation-message'>" . $major_value[$major_key] . "</div>";
        $birth_response = "<div class='confirmation-message'>" . $birth . "</div>";
        $address_response = "<div class='confirmation-message'>" . $address . "</div>";

        if ($_SERVER['REQUEST_METHOD'] === 'POST')
            // Check if a file was uploaded
            if (isset($_FILES['img']) && $_FILES['img']['error'] === UPLOAD_ERR_OK)
                move_uploaded_file($_FILES['img']['tmp_name'], "upload.png");
            else
                if (file_exists("upload.png"))
                    unlink("upload.png");
    ?>
    <div class="container">
        <div class="form-border">
        <div id="form-error" class="error-message"></div>
            <form action="confirm.php" id="form-login" method = "POST">
                <div class="form-group">
                    <label for="name" class="form-label">Họ và tên</label>
                    <?php echo "$name_response"?>
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Giới tính</label>
                    <?php echo "$gender_response"?>
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Phân khoa</label>
                    <?php echo "$major_response"?>
                </div>
                <div class="form-group">
                    <label for="birth" class="form-label">Ngày sinh</label>
                    <?php echo "$birth_response"?>
                </div>
                <div class="form-group">
                    <label for="address" class="form-label" style>Địa chỉ</label>
                    <?php echo "$address_response"?>
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Hình ảnh</label>
                    <img src="upload.png">
                </div>
            </form>
        </div>
    </div>
</body>
</html>