<html>

<head>
	<title>Đăng nhập</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
	<div class="mx-auto my-auto border d-flex flex-column" style="max-width: 550px;border: 2px solid #2980D7!important;">

		<?php date_default_timezone_set("Asia/Ho_Chi_Minh");
		$time = date("H:i");
		$day_of_week = date("N") + 1;
		$dmy = date("d/m/Y");

		echo '<form class="my-3 d-flex flex-column mx-auto">
				<div class="mb-2 px-1" style="background: #f2f2f2; font-family: Times New Roman, serif;">
					<span>Bây giờ là: '.$time.', thứ '.$day_of_week.' ngày '.$dmy.'</span>
				</div>
				
				<table>	
					<tr>
						<td><div style="display: inline-block; width: 150px; background-color: rgb(68, 132, 195); border: 2px solid rgb(29, 109, 171);">
                                			<label for="username" style="color: white; font-family: Times New Roman, serif;">Tên đăng nhập</label></div>
                        			</td>
                        			<td><input type="text" id="username" style="height: 32px; border: 2px solid rgb(29, 109, 171); font-family: Times New Roman, serif;"></td>
                    			</tr>
			
					<tr>
						<td><div style="display: inline-block; width: 150px; background-color: rgb(68, 132, 195); border: 2px solid rgb(29, 109, 171);">
                                			<label for="username" style="color: white; font-family: Times New Roman, serif;">Mật khẩu</label></div>
                        			</td>
                        			<td><input type="password" id="password" style="height: 32px; border: 2px solid rgb(29, 109, 171); font-family: Times New Roman, serif;"></td>
                    			</tr>
				</table>

				<button type="submit" style="width: 120px; font-family: Times New Roman, serif; background-color: rgb(68, 132, 195); border: 2px solid rgb(29, 109, 171);" class="mx-auto btn btn-primary mt-3">
					Đăng nhập
				</button>
			</form>' 
		?>
	</div>
</body>

</html>