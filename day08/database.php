<?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $database = "ltweb";

        $conn = new mysqli($servername, $username, $password, $database);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        $hoTen = $_POST['name'];
        $gioiTinh = $_POST['gender'];
        $phanKhoa = $_POST['major'];
        $ngaySinh = date('Y-m-d', strtotime(str_replace('/', '-', $_POST['birth'])));
        $diaChi = $_POST['address'];
        $hinhAnh = $_POST['img'];

        $stmt = $conn->prepare("INSERT INTO SINHVIEN (HoTen, GioiTinh, PhanKhoa, NgaySinh, DiaChi, HinhAnh) VALUES (?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("ssssss", $hoTen, $gioiTinh, $phanKhoa, $ngaySinh, $diaChi, $hinhAnh);
        $stmt->execute();

        if ($stmt->affected_rows > 0) {
            echo "Dữ liệu được lưu thành công!";
        } else {
            echo "Dữ liệu lưu thất bại";
            unlink($hinhAnh);
        }

        $stmt->close();
        $conn->close();
    }
?>