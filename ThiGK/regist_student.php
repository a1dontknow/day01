<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Confirm</title>
    <style>
	.container{
    	display: flex;
		justify-content: center;
	}
	.form-border{
	    height: 800px;
    	width: 500px;
		border: 1.5px solid #1a89ba;
    }
    .form-group{
        display: flex;
        align-items: center;
        justify-content: flex-start;
        padding: 2%;
        margin-left: 10%;
    }

    .form-label{
        background-color: #6ca834;
        border: 1.5px solid #1a89ba;
        min-width: 18%;
        margin-right: 5%;
        padding: 1%;
        color: white;
    }

    .form-control {
        background-color: #fff;
        border: 0px solid #1a89ba;
        display: block;
        width: 52%;
        padding: 0.3rem 0.9rem;
        font-size: 1rem;
        color: #495057;
      }

    .btn-submit{
        background-color: #6ca834;
        border: 1.5px solid #1a89ba;
        width: 30%;
        color: white;
        cursor: pointer;
        margin-left: 140px;
    }
    .form-group input[type="radio"] {
        width: 10%;
    }

    img {
        width: 300px;
        border: 1px solid #ccc;
    }

    </style>
</head>

<body>
    <?php
        // Retrieve form data
        $name = $_POST['name'];
        $gender = $_POST['gender'];
        $year = $_POST['year'];
        $day = $_POST['day'];
        $month = $_POST['month'];
        $city = $_POST['city'];
        $district = $_POST['district'];
        $other_info = $_POST['info'];

        $date = sprintf("%02d/%02d/%04d", $day, $month, $year);
        $address = sprintf("%s - %s",$district, $city);
    ?>

    <div class="container">
        <div class="form-border">
            <form action="database.php" id="form-login" method="POST">
                <div class="form-group">
                    <label for="name" class="form-label">Họ và tên</label>
                    <input type="text" name="name" class="form-control" value="<?php echo $name; ?>" readonly>
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Giới tính</label>
                    <input type="text" name="gender" class="form-control" value="<?php echo $gender; ?>" readonly>
                </div>
                <div class="form-group">
                    <label for="birth" class="form-label">Ngày sinh</label>
                    <input type="text" name="birth" class="form-control" value="<?php echo $date; ?>" readonly>
                </div>
                <div class="form-group">
                    <label for="address" class="form-label">Ngày sinh</label>
                    <input type="text" name="address" class="form-control" value="<?php echo $address; ?>" readonly>
                </div>

                <div class="form-group">
                    <label for="other_info" class="form-label" style>Thông tin khác</label>
                    <textarea type="text" id="other_info" name="other_info" class="form-control" style="width: 223px; height: 100px; padding: 1px; margin-top:2px; font-family: Arial; padding: 0.3rem 0.9rem;" readonly><?php echo $other_info; ?></textarea>
                </div>
            </form>
        </div>
    </div>
</body>
</html>