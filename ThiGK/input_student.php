<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Thông tin đăng ký</title>
    <style>
	.container{
    	display: flex;
		justify-content: center;
	}
	.form-border{
	    height: 600px;
    	width: 500px;
		border: 1.5px solid #1a89ba;
    }
    .form-group{
        display: flex;
        align-items: center;
        justify-content: flex-start;
        padding: 2%;
        margin-left: 10%;
    }
    .form-label{
        background-color: #6ca834;
        border: 1.5px solid #1a89ba;
        margin-right: 5%;
        padding: 1%;
        color: white;
    }

    .form-label{
        background-color: #6ca834;
        border: 1.5px solid #1a89ba;
        min-width: 22%;
        margin-right: 5%;
        padding: 1%;
        color: white;
    }

    .form-control {
        background-color: #fff;
        border: 1.5px solid #1a89ba;
        display: block;
        width: 52%;
        padding: 0.3rem 0.9rem;
        font-size: 1rem;
        color: #495057;
      }

    .btn-submit{
        background-color: #6ca834;
        border: 1.5px solid #1a89ba;
        width: 30%;
        color: white;
        cursor: pointer;
        margin-left: 140px;
    }
    .form-group input[type="radio"] {
        width: 10%;
    }

    .error_message {
        color: red;
        margin-left: 60px;
    }

    </style>

</head>

<body>
    <div class="container">
        <div class="form-border">
        <div id="form-error" class="error-message"></div>
            <form action="regist_student.php" id="form-login" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name" class="form-label">Họ và tên</label>
                    <input type="text" id="name" name="name" class="form-control">
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Giới tính</label>
                    <?php
                        $gender = [
                            0 => "Nam",
                            1 => "Nữ",
                        ];
                        $genderKeys = array_keys($gender);
                        $genderValues = array_values($gender);

                        for ($i = 0; $i < count($gender); $i++) {
                            $key = $genderKeys[$i];
                            $value = $genderValues[$i];

                            echo '<input type="radio" id="gender-' . $key . '" value="' . $value . '" class="form-control" name="gender">';
                            echo '<label for="gender-' . $key . '">' . $value . '</label>';
                        }
                    ?>
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Ngày sinh</label>
                    <select name="year" id="year">
                        <option value="">Năm</option>
                        <?php
                            $currentYear = date('Y');
                            for ($i = $currentYear - 40; $i <= $currentYear - 15; $i++) {
                              echo "<option value='$i'>$i</option>";
                            }
                        ?>
                    </select>

                    <select name="month" id="month">
                        <option value="">Tháng</option>
                          <?php
                              for ($i = 1; $i <= 12; $i++) {
                                $month = str_pad($i, 2, '0', STR_PAD_LEFT);
                                echo "<option value='$month'>$month</option>";
                              }
                          ?>
                    </select>

                    <select name="day" id="day">
                        <option value="">Ngày</option>
                        <?php
                            for ($i = 1; $i <= 31; $i++) {
                              $day = str_pad($i, 2, '0', STR_PAD_LEFT);
                              echo "<option value='$day'>$day</option>";
                            }
                        ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="" class="form-label">Địa chỉ</label>
                    <select name="city" id="city">
                        <option value="">Thành phố</option>
                        <option value="Hà Nội">Hà Nội</option>
                        <option value="Tp. Hồ Chí Minh">Tp. Hồ Chí Minh</option>
                    </select>

                    <select name="district" id="district">
                        <option value="">Quận</option>
                    </select>

                    <script>
                        const citySelect = document.getElementById("city");
                        const districtSelect = document.getElementById("district");


                        const districtsByCity = {
                            "Hà Nội": ["Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"],
                            "Tp. Hồ Chí Minh": ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"],
                        };

                        citySelect.addEventListener("change", () => {
                            const selectedCity = citySelect.value;
                            const districtList = districtsByCity[selectedCity] || [];

                            districtSelect.innerHTML = '<option value="">Quận</option>';

                            districtList.forEach((district) => {
                                const option = document.createElement("option");
                                option.value = district;
                                option.textContent = district;
                                districtSelect.appendChild(option);
                            });
                        });
                    </script>
                </div>

                <div class="form-group">
                    <label for="info" class="form-label" style ="margin-bottom:70px;">Địa chỉ</label>
                    <textarea type="text" id="info" name="info" class="form-control" style="width: 223px; height: 100px; padding: 1px; margin-top:2px; font-family: Arial; padding: 0.3rem 0.9rem;"></textarea>
                </div>

                <div class="form-group">
                    <input type="submit" class="btn-submit form-control" value="Đăng ký">
                </div>

            </form>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $('form').submit(function(event) {
              $(".error_message").remove()
              if ($('#name').val() === '') {
                event.preventDefault();
                $('#form-error').append("<p class='error_message'>Hãy nhập họ tên.</p>");
              }

              if ($('input[name="gender"]:checked').length === 0) {
                event.preventDefault();
                $('#form-error').append("<p class='error_message'>Hãy chọn giới tính.</p>");
              }

              if ($('#year').val() === '' || $('#month').val() === '' || $('#day').val() === '') {
                event.preventDefault();
                $('#form-error').append("<p class='error_message'>Hãy chọn ngày sinh.</p>");
              }

              if ($('#city').val() === '' || $('#district').val() === '') {
                event.preventDefault();
                $('#form-error').append("<p class='error_message'>Hãy chọn địa chỉ.</p>");
              }
        });
      });
    </script>

</body>
</html>
