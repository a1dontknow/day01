<?php

    $major = [
        "0" => "None",
        "MAT" => "Khoa học máy tính",
        "KDL" => "Khoa học vật liệu"
    ];

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $major = $major[$_POST['major']];
        $keyword = '%a%' ;
        $servername = "localhost";
        $username = "root";
        $password = "";
        $database = "ltweb";

        $conn = new mysqli($servername, $username, $password, $database);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        if ($major == "None" && $keyword == "") {
            $sql = "SELECT * FROM sinhvien";
            $stmt = $conn->prepare($sql);
        }
        else if ($major == "None") {
            $sql = "SELECT * FROM sinhvien WHERE sinhvien.HoTen LIKE CONCAT('%', CONCAT(?, '%'))";
            $stmt = $conn->prepare($sql);
            $stmt->bind_param("s", $keyword);
        }
        else if ($keyword == "") {
            $sql = "SELECT * FROM sinhvien WHERE sinhvien.PhanKhoa LIKE ?";
            $stmt = $conn->prepare($sql);
            $stmt->bind_param("s", $major);
        }
        else {
            $sql = "SELECT * FROM sinhvien WHERE sinhvien.PhanKhoa LIKE ? AND sinhvien.HoTen LIKE CONCAT('%', CONCAT(?, '%'))";
            $stmt = $conn->prepare($sql);
            $stmt->bind_param("ss", $major, $keyword);
        }

        $stmt->execute();
        $result = $stmt->get_result();
        $row = '';
        for ($i = 1; $i <= $result->num_rows; $i++) {
            $record = $result->fetch_assoc();
            $row .= ' <tr>
                <th scope="row">'.$i.'</th>
                <td>'.$record['HoTen'].'</td>
                <td>'.$record['PhanKhoa'].'</td>
                <td>
                    <button onclick="confirmDeleteAction('<?php echo $student['MaSV']; ?>')" type="button" class="btn btn-primary">Xóa</button>
                    <button type="button" class="btn btn-primary">Sửa</button>
                </td>
            </tr>';
        }
        echo json_encode(array("status"=> "success","message"=> "successfully!", "row" => $row, "num" => $result->num_rows));
    }

?>