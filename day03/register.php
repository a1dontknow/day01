<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Register</title>
    <style>
	.container{
    	display: flex;
		justify-content: center;
	}
	.form-border{
	    height: 250px;
    	width: 500px;
		border: 1.5px solid #1a89ba;
    }
    .form-group{
        display: flex;
        align-items: center;
        justify-content: flex-start;
        padding: 2%;
        margin-left: 10%;
    }
    .form-label{
        background-color: #5d9dd5;
        border: 2px solid #1a89ba;
        min-width: 18%;
        margin-right: 5%;
        padding: 1%;
        color: white;
    }
    .form-control {
        background-color: #fff;
        border: 1.5px solid #1a89ba;
        display: block;
        width: 50%;
        padding: 0.3rem 0.9rem;
        font-size: 1rem;
        color: #495057;
      }
    .btn-submit{
        background-color: #6ca834;
        border: 1.5px solid #1a89ba;
        width: 30%;
        color: white;
        cursor: pointer;
        margin-left: 140px;
    }
    .form-group input[type="radio"] {
        width: 10%;
    }
    </style>

</head>

<body>
    <?php
        $gender = [
            0 => "Nam",
            1 => "Nữ",
        ];
        $major = [
            "MAT" => "Khoa học máy tính",
            "KDL" => "Khoa học vật liệu",
        ];
    ?>

    <div class="container">
        <div class="form-border">
            <form action="" id="form-login">
                <div class="form-group">
                    <label for="name" class="form-label">Họ và tên</label>
                    <input type="text" id="name" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Giới tính</label>
                    <?php
                        $genderKeys = array_keys($gender);
                        $genderValues = array_values($gender);

                        for ($i = 0; $i < count($gender); $i++) {
                            $key = $genderKeys[$i];
                            $value = $genderValues[$i];

                            echo '<input type="radio" id="' . $key . '" value="' . $value . '" class="form-control" name="gender">';
                            echo '<label for="gender-' . $key . '">' . $value . '</label>';
                        }
                    ?>
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Phân khoa</label>
                    <select name="" id="major" class="form-control">
                        <option value="0" selected>--Chọn phân khoa--</option>
                    <?php
                        foreach ($major as $key => $value) {
                            echo '<option value="' . $key . '">' . $value . '</option>';
                        }
                    ?>
                    </select>
                </div>  
                <div class="form-group">
                    <input type="submit" class="btn-submit form-control" value="Đăng ký">
                </div>         
            </form>
        </div>
    </div>
</body>
</html>