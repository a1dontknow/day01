-- Tạo bảng

CREATE DATABASE IF NOT EXISTS QLSV;
USE QLSV;

CREATE TABLE DMKHOA (
    MaKH VARCHAR(6) PRIMARY KEY,
    TenKhoa VARCHAR(30)
);

CREATE TABLE SINHVIEN (
    MaSV VARCHAR(6) PRIMARY KEY,
    HoSV VARCHAR(30),
    TenSV VARCHAR(15),
    GioiTinh CHAR(1),
    NgaySinh DATETIME,
    NoiSinh VARCHAR(50),
    DiaChi VARCHAR(50),
    MaKH VARCHAR(6) REFERENCES DMKHOA(MaKH),
    HocBong int
);

-- Sinh dữ liệu
INSERT INTO dmkhoa VALUES ('TCT', 'Toán - Cơ - Tin học'),
						  ('VL', 'Vật lý'),
                          ('DC', 'Địa chất'),
                          ('CNTT', 'Công nghệ thông tin');
                          
INSERT INTO sinhvien VALUES ('100001', 'Sinh viên', '1', 'M', '1998-01-23 12:45:56', 'Hà Nội', 'Địa chỉ 1', 'TCT', '1000'),
							('100002', 'Sinh viên', '2', 'M', '1998-01-24 12:45:57', 'Hòa Bình', 'Địa chỉ 2', 'VL', '2000'),
                            ('100003', 'Sinh viên', '3', 'F', '1998-01-25 12:45:58', 'Hải Phòng', 'Địa chỉ 3', 'VL', '3000'),
                            ('100004', 'Sinh viên', '4', 'F', '1998-01-26 12:45:59', 'Huế', 'Địa chỉ 4', 'CNTT', '4000'),
                            ('100005', 'Sinh viên', '5', 'M', '1998-01-27 12:46:00', 'Đà Nẵng', 'Địa chỉ 5', 'CNTT', '5000'),
                            ('100006', 'Sinh viên', '6', 'M', '1998-01-28 12:47:56', 'Nghệ An', 'Địa chỉ 6', 'DC', '6000'),
                            ('100007', 'Sinh viên', '7', 'F', '1998-01-29 12:48:56', 'Hà Nội', 'Địa chỉ 7', 'CNTT', '7000');

-- Lấy ra toàn bộ sinh viên thuộc khoa "Công nghệ thông tin"

SELECT * 
FROM sinhvien NATURAL JOIN dmkhoa
WHERE TenKhoa = "Công nghệ thông tin"